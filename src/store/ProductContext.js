import React, { useState, useEffect, createContext } from 'react'
import { data } from '../data/data';

export const ProductContext = createContext();

export const ProductProvider = props => {
    const [products, setProducts] = useState([])

    useEffect(() => {
        setProducts(data)
    }, [])

    return (
        <ProductContext.Provider value={[products, setProducts]}>
            {props.children}
        </ProductContext.Provider>
    );
}

