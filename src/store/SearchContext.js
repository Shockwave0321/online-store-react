import React, { useState, createContext } from 'react'

export const SearchContext = createContext();

export const SearchProvider = props => {
    const [keyword, setKeyword] = useState('')

    return (
        <SearchContext.Provider value={[keyword, setKeyword]}>
            {props.children}
        </SearchContext.Provider>
    )
}

