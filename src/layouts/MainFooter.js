import React from 'react'
import { Layout } from 'antd';

const { Footer } = Layout;
const MainFooter = () => {
    return (
        <Footer style={{ textAlign: 'center' }}>Enrique MotoShop copyright@2020</Footer>
    )
}

export default MainFooter
