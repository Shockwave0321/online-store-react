import React from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import Cart from './components/Cart';
import Home from './pages/Home';
import Navbar from './components/Navbar';
import ProductList from './components/ProductList';
import ProductDetail from './pages/ProductDetail';
import { CartProvider } from './store/CartContext';
import { ProductProvider } from './store/ProductContext';
import { SearchProvider } from './store/SearchContext';
import MainFooter from './layouts/MainFooter';

import 'antd/dist/antd.css';
const App = () => {
    return (
      <CartProvider>
          <ProductProvider>
              <BrowserRouter>
                <Navbar />
                <div className="container">
                  <div className="mt-5">
                    <SearchProvider>
                      <Switch>
                        <Route path='/' exact component={Home} />
                        <Route path='/cart' component={Cart} />
                        <Route path='/products' component={ProductList} />
                        <Route path='/product-detail/:productId' component={ProductDetail} />
                      </Switch>
                    </SearchProvider>
                  </div>
                </div>
                <MainFooter />
              </BrowserRouter>
          </ProductProvider>
      </CartProvider>  
    )
}

export default App
