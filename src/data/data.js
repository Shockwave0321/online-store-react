export const data = [
        {
            "id": 1,
            "brand": "M4",
            "name": "M4 Standard High Sport Mount Full System Exhaust - Single",
            "price": 47809.45,
            "category": "Exhaust",
            "imageUrl": "https://cdn11.bigcommerce.com/s-coxd9/images/stencil/1280x1280/products/66939/367436/M4_Exhaust_Cannister_Generic__13962.1453919208.jpg?c=2&imbypass=on",
            "rating" : 5,
            "description": `M4 Exhaust manufactures exhausts which add power and a more race-inspired 
            sound to your bike as well as lowering the overall weight. 
            Extra time on research and development ensures your exhaust has 
            the ultimate in quality workmanship, ease of installation, 
            and the best performance possible for your motorcycle. M4 Exhausts are available 
            in both Standard / Race and GP Series mounting configurations. 
            Step up your motorcycle's style and power with the M4 Standard Full System Exhaust.`
        },
        {
            "id": 2,
            "brand": "CRG",
            "name": "CRG RC2 Brake Lever",
            "price": 809.45,
            "category": "Handlebars",
            "imageUrl": "https://www.cyclegear.com/_a/product_images/0132/6934/crgrc2_brake_lever_zx6_r_daytona_speed_triple_street_triple_rgsxr_black.jpg",
            "rating" : 3,
            "description": `The RC2 Brake Lever provides riders with great looks, better 
            ergonomics and on the fly position adjustment that can be used to combat brake fade`
        },
        {
            "id": 3,
            "brand": "Brembo",
            "name": "Brembo 484 Cafe Racer Brake Calipers",
            "price": 15809.15,
            "category": "Brakes",
            "imageUrl": "https://www.brembo.com/en/PublishingImages/moto/performance/pinza-484/223.jpg",
            "rating" : 2,
            "description": `The Brembo 484 Brake Calipers feature the same world class performance 
            and components that Brembo is known for, now in a more subtle and classic looking package. 
            Its black anodized finish combined with the handpainted Brembo “b” logo ensures this caliper 
            will match your classic cafe or custom. The caliper consists of two CNC machined aluminum
            halves mated together for an ultra rigid construction`
        },
        {
            "id": 4,
            "brand": "GALFER BRAKING SYSTEMS",
            "name": "Galfer Wave Rotor Front DF912",
            "price": 20500.45,
            "category": "Brakes",
            "imageUrl": "https://www.revzilla.com/product_images/0366/1278/galfer_wave_rotors_front_750x750.jpg",
            "rating" : 4,
            "description": "Galfer Wave Brake Rotors are laser cut from a proprietary high-carbon 420 Stainless Steel that has been heat treated and parallel diamond ground to ensure a perfect surface. The rotors float on an 7071 Aluminum center carrier to ensure optimum pad to rotor contact for maximum stopping force and feel."
        },
        {
            "id": 5,
            "brand": "GALFER BRAKING SYSTEMS",
            "name": "Galfer Wave Rotor Back DF912",
            "price": 20500.45,
            "category": "Brakes",
            "imageUrl": "https://www.revzilla.com/product_images/0366/1278/galfer_wave_rotors_front_750x750.jpg",
            "rating" : 4,
            "description": "Galfer Wave Brake Rotors are laser cut from a proprietary high-carbon 420 Stainless Steel that has been heat treated and parallel diamond ground to ensure a perfect surface. The rotors float on an 7071 Aluminum center carrier to ensure optimum pad to rotor contact for maximum stopping force and feel."
        }
    ]
