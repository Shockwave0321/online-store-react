import React, { useState, useEffect, useContext } from 'react'
import { Layout, Row, Col } from 'antd';
import { ProductContext } from '../store/ProductContext';
import Category from '../components/Category';
import NavMap from '../components/NavMap';

const { Header, Footer, Sider, Content } = Layout;

const ProductDetail = props => {
    const [products, setProducts] = useContext(ProductContext);
    const [productDetail, setProductDetail] = useState({});

    useEffect(() => {
  
        let productObject = products.find(product => product.id == props.match.params.productId)
        setProductDetail(prevState => productObject)

    }, [])

    return (
        <Layout style={{ backgroundColor: '#ffff' }}>
            <br /><br />
            <NavMap/>
            <br /><br />
            <Layout style={{ backgroundColor: '#ffff' }}>
                <Content>
                    
                <Row>
                    <Col span={5}>
                        <Category />
                    </Col>
                    
                    <Col span={12}>

                        <Header style={{ backgroundColor: '#ffff' }}>
                            <h2>{productDetail.name}</h2> 
                            <h3>Php {productDetail.price}</h3>
                        </Header>
                        <br /><br />
                        <div style={{ margin: '50px' }}>
                            <img src={productDetail.imageUrl} />
                        </div>
                        
                    </Col>

                    <Col span={7}>
                        <h3>Product Details</h3>

                        <p>{productDetail.description}</p>
                    </Col>
                </Row>

                </Content>
             
            </Layout>
         
        </Layout>
    )
}

export default ProductDetail
