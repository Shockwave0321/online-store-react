
import React, { useReducer } from 'react'
import { Link } from 'react-router-dom';
import Search from '../components/Search'
import ProductList from '../components/ProductList';
import Categories from '../components/Categories';
import Slider from '../components/Slider';
import StarredProduct from '../components/StarredProduct';
import FlexContainer from '../components/FlexContainer';

const Home = () => {

    return (
        <div class="mt-5">
            <br />
            <Categories />
            <br />
            <Slider />
           
            <FlexContainer />
            <br />
        </div>
    )
}

export default Home
