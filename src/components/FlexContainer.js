import React from 'react'
import { Row, Col, Divider } from 'antd';
import StarredProduct from './StarredProduct';
import '../styles/product.css';

const FlexContainer = () => {

    const showProductDetail = () => {
        
    }

    return (
        <div className="product-container">
            <StarredProduct 
                name="Woomdee DashCam" 
                description="Bike Ultra HD Dash Cam" 
                imgUrl="https://images-na.ssl-images-amazon.com/images/I/61yr1aGYNvL._SY355_.jpg" 
                onClick={showProductDetail()}
            />
            <StarredProduct productId="1" name="BC1080 Plus" description="1080 Plus Dash Cam" imgUrl="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS9GXhhgpoZ3g7tn4xRLSPnHHwDntbs4w_0HnyuuUeRnOTXmsQc&usqp=CAU" />
            <StarredProduct productId="2" name="Woomdee DashCam" description="Bike Ultra HD Dash Cam" imgUrl="https://images-na.ssl-images-amazon.com/images/I/61yr1aGYNvL._SY355_.jpg" />
            <StarredProduct productId="3" name="BC1080 Plus" description="Kamote 1080 Plus Dash Cam" imgUrl="https://images-na.ssl-images-amazon.com/images/I/61-lFbe4aSL._AC_SX425_.jpg" />
            <StarredProduct productId="4" name="Woomdee DashCam" description="Bike Ultra HD Dash Cam" imgUrl="https://images-na.ssl-images-amazon.com/images/I/61yr1aGYNvL._SY355_.jpg" />
            <StarredProduct productId="5" name="BC1080 Plus" description="Kamote 1080 Plus Dash Cam" imgUrl="https://images-na.ssl-images-amazon.com/images/I/61-lFbe4aSL._AC_SX425_.jpg" />
            <StarredProduct productId="6" name="Woomdee DashCam" description="Bike Ultra HD Dash Cam" imgUrl="https://images-na.ssl-images-amazon.com/images/I/61yr1aGYNvL._SY355_.jpg" />
            <StarredProduct productId="7" name="BC1080 Plus" description="Kamote 1080 Plus Dash Cam" imgUrl="https://images-na.ssl-images-amazon.com/images/I/61-lFbe4aSL._AC_SX425_.jpg" />
        </div>
    )
}

export default FlexContainer
