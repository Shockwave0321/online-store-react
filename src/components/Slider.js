import React from 'react'
import { Carousel } from 'antd';
import '../styles/slider.css';

const Slider = () => {
    return (
        <div className="carousel-main"> 
            <Carousel autoplay>
                <div>
                    <img src="https://cdn.motor1.com/images/mgl/vqYYK/s1/gsxr1000r.jpg" />
                </div>
                <div>
                    <img src="https://www.a1autorentalgd.com/wp-content/uploads/2019/04/ducati-1198s.jpg" />
                </div>
                <div>
                    <img src="https://www.webbikeworld.com/wp-content/uploads/2020/04/2020-ducati-scrambler-icon-dark-06-682x394.jpg" />
                </div>
        
            </Carousel>
        </div>
    )
}

export default Slider