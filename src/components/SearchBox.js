import React, { useContext } from 'react'
import { useHistory  } from "react-router-dom";
import { Input } from 'antd';
import { SearchContext } from '../store/SearchContext';

const { Search } = Input;

const SearchBox = () => {
    const [keyword, setKeyword] = useContext(SearchContext)

    let history = useHistory()
    
    const searchProduct = (value) => {
        if(value){
            setKeyword(value)
            history.push('/products')
        }
    }

    return (
        <Search
            placeholder="search product"
            onSearch={value => searchProduct(value)}
            style={{ width: 250, marginLeft: '20px' }} 
        />
    )
}

export default SearchBox
