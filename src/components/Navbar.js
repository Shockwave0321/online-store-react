import React, { useState, useContext } from 'react'
import { Link } from 'react-router-dom';
import { CartContext } from '../store/CartContext';
// import '../styles/menu.scss';

const Navbar = () => {
    const [items, setItems] = useContext(CartContext)
    const style = {
        position: 'absolute', 
        transform: `translate3d(${0}px, ${38}px, ${0}px)`, 
        top: '0px', 
        left: '0px',
        willChange: 'transform'
    }

    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-dark bg-primary fixed-top">
                <Link className="navbar-brand" to="/">Enrique MotoShop</Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>

             

                <div className="collapse navbar-collapse" id="navbarColor01">
                
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <Link to="/cart" className="btn btn-outline-success">
                                <i className="fas fa-store-slash"></i> My Cart : {items.length}
                            </Link>
                        </li>

                        <li className="nav-item ml-2">
                            <button type="button" className="btn btn-outline-info">Sign In</button>
                        </li>
                    </ul>
                    
                </div>
            </nav>
        </div>
    )
}

export default Navbar