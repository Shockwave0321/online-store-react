import React from 'react';
import { Menu } from 'antd';
import { AppstoreOutlined, MailOutlined, SettingOutlined } from '@ant-design/icons';

const { SubMenu } = Menu;

class Category extends React.Component {
  // submenu keys of first level
  rootSubmenuKeys = ['sub1', 'sub2', 'sub4'];

  state = {
    openKeys: ['sub1'],
  };

  onOpenChange = openKeys => {
    const latestOpenKey = openKeys.find(key => this.state.openKeys.indexOf(key) === -1);
    if (this.rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      this.setState({ openKeys });
    } else {
      this.setState({
        openKeys: latestOpenKey ? [latestOpenKey] : [],
      });
    }
  };

  render() {
    return (
      <Menu
        mode="inline"
        openKeys={this.state.openKeys}
        onOpenChange={this.onOpenChange}
        style={{ width: 256 }}
      >
        <SubMenu
          key="sub1"
          title={
            <span>
              <MailOutlined />
              <span>Brands</span>
            </span>
          }
        >
          <Menu.Item key="1">Honda</Menu.Item>
          <Menu.Item key="2">Kawasaki</Menu.Item>
          <Menu.Item key="3">Yamaha</Menu.Item>
          <Menu.Item key="4">Ducati</Menu.Item>
          <Menu.Item key="3">Suzuki</Menu.Item>
          <Menu.Item key="4">Harley Davidson</Menu.Item>
        </SubMenu>
        <SubMenu key="sub2" icon={<AppstoreOutlined />} title="Categories">
          <Menu.Item key="5">Helmets</Menu.Item>
          <Menu.Item key="6">Pads</Menu.Item>
          <Menu.Item key="6">Devices</Menu.Item>
          <SubMenu key="sub3" title="Submenu">
            <Menu.Item key="7">GPS</Menu.Item>
            <Menu.Item key="8">Camera</Menu.Item>
          </SubMenu>
        </SubMenu>
        <SubMenu key="sub4" icon={<SettingOutlined />} title="Price Range">
          <Menu.Item key="9">50,000 - 100, 000</Menu.Item>
          <Menu.Item key="10">100,000 - 150,000</Menu.Item>
          <Menu.Item key="11">150,000 - 200,000</Menu.Item>
        </SubMenu>
      </Menu>
    );
  }
}

export default Category