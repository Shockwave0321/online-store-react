import React from 'react'
import { Breadcrumb } from 'antd';

const NavMap = () => {
    return (
        <Breadcrumb separator=">">
            <Breadcrumb.Item>Home</Breadcrumb.Item>
            <Breadcrumb.Item href="">Product List</Breadcrumb.Item>
            <Breadcrumb.Item>Product Details</Breadcrumb.Item>
        </Breadcrumb>
    )
}

export default NavMap
