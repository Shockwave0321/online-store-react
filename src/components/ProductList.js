import React, { useContext, useState, useEffect } from 'react'
import Product from './Product';
import { ProductContext } from '../store/ProductContext';
import { CartContext } from '../store/CartContext';
import { SearchContext } from '../store/SearchContext';
import '../styles/product.css';

const ProductList = () => {
    const [products, setProducts] = useContext(ProductContext);
    const [items, setItems] = useContext(CartContext)
    const [keyword, setKeyword] = useContext(SearchContext)

    const [result, setResult] = useState([])
  
    useEffect(() => {
        let filtered = products.filter(product => product.name.toLowerCase().includes(keyword.toLowerCase()))
        return setResult(prevState => [...prevState, filtered])
        // console.log(filtered)
    }, [])
   
    const addProductToCart = (product) => {
        setItems(prevItem => [...prevItem, {
            brand: product.brand,
            name: product.name,
            price: product.price,
            category: product.category,
            imageUrl: product.imageUrl,
            rating: product.rating,
            description: product.description
        }])
    
    }
   
    return (
        <div className="container">
            <div className="row">
                <div className="mt-5">
                    <h4>Search Filter: {keyword}</h4>
                  
                </div>
            </div>
            <div className="product-container">
                {
                    result.map((product, index) => {
                        return <Product onClick={e => addProductToCart(product)} 
                                    key={index} 
                                    name={product.name} 
                                    price={product.price} 
                                    brand={product.brand} 
                                    category={product.category}
                                    imageUrl={product.imageUrl}
                                    rating={product.rating}
                                    description={product.description} 
                                />
                    })
                }
            </div>
        </div>
    )
}

export default ProductList
