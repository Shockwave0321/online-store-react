import React from 'react'
import { Link } from 'react-router-dom';
import { Select } from 'antd';

const { Option } = Select;

    const onChange = (value) => {
    console.log(`selected ${value}`);
  }
  
  const onBlur = () => {
    console.log('blur');
  }
  
  const onFocus = () => {
    console.log('focus');
  }
  
  const onSearch = (val) => {
    console.log('search:', val);
  }

  
const Search = () => {
    return (
         <div class="card border-secondary mb-3 w-75 m-auto">
            <div class="card-header">Search Bike</div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-4">

                    <Select
                        showSearch
                        style={{ width: 200 }}
                        placeholder="Select a person"
                        optionFilterProp="children"
                        onChange={onChange}
                        onFocus={onFocus}
                        onBlur={onBlur}
                        onSearch={onSearch}
                        filterOption={(input, option) =>
                        option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
                        }
                    >
                        <Option value="jack">Jack</Option>
                        <Option value="lucy">Lucy</Option>
                        <Option value="tom">Tom</Option>
                    </Select>

                    </div>
                    <div class="col-sm-4">
                      
                        <div class="form-group">
                            <select class="custom-select">
                            <option selected="">Make</option>
                            <option value="1">Ducati</option>
                            <option value="2">Kawasaki</option>
                            <option value="3">Yamaha</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                     
                        <div class="form-group">
                            <select class="custom-select">
                            <option selected="">Model</option>
                            <option value="1">Scrambler</option>
                            <option value="2">Ninja</option>
                            <option value="3">XSR900</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="row mt-2">
                    {/* <button 
                        type="button" 
                        class="btn btn-primary m-auto"
                     >
                        Search
                    </button> */}
                    <Link className="btn btn-primary m-auto" to="/products">Search motorcycle parts</Link>
                </div>

               
            </div>
        </div>
    )
}

export default Search
