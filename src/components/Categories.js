import React from 'react';
import { Menu } from 'antd';
import { MailOutlined, CloudSyncOutlined, SettingOutlined } from '@ant-design/icons';
import { Link } from "react-router-dom";

import SearchBox from './SearchBox';

const { SubMenu } = Menu;

class Categories extends React.Component {
  state = {
    current: 'mail',
  };

  handleClick = e => {
    console.log('click ', e);
    this.setState({
      current: e.key,
    });
  };

  render() {
    return (
      <Menu onClick={this.handleClick} selectedKeys={[this.state.current]} mode="horizontal">
        <Menu.Item key="mail" icon={<MailOutlined />}>
          Helmets
        </Menu.Item>
        <SubMenu icon={<SettingOutlined />} title="Riding Gears">
          <Menu.ItemGroup title="Gadgets">
            <Menu.Item key="setting:1">Dash Cams</Menu.Item>
            <Menu.Item key="setting:2">GPS</Menu.Item>
          </Menu.ItemGroup>
          <Menu.ItemGroup title="Gears">
            <Menu.Item key="setting:3">Pad</Menu.Item>
            <Menu.Item key="setting:4">Gloves</Menu.Item>
          </Menu.ItemGroup>
        </SubMenu>
        <SubMenu icon={<CloudSyncOutlined />} title="Accessories">
          <Menu.ItemGroup title="Gadgets">
            <Menu.Item key="setting:1">Dash Cams</Menu.Item>
            <Menu.Item key="setting:2">GPS</Menu.Item>
          </Menu.ItemGroup>
          <Menu.ItemGroup title="Gears">
            <Menu.Item key="setting:3">Pad</Menu.Item>
            <Menu.Item key="setting:4">Gloves</Menu.Item>
          </Menu.ItemGroup>
        </SubMenu>
        <Menu.Item key="alipay">
            Tires
        </Menu.Item>
        <Menu.Item key="mufflers">
            <Link to="/product-detail">Mufflers</Link>
        </Menu.Item>
          
            <SearchBox />

      </Menu>
    );
  }
}

export default Categories