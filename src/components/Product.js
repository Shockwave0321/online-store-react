import React from 'react'
import RatingIcon from './RatingIcon'
import '../styles/product.css';

const Product = ({ brand, name, price, imageUrl, rating, onClick}) => {

    return (
            <div className="card m-3" style={{width: '320px', height: '400px'}}>
                <div className="card-body">
                    <h4 className="card-title">{name}</h4>
                    <h5 className="card-subtitle mb-2 text-muted">{brand}</h5>
                    <h6 className="card-subtitle mb-2 text-muted">PHP {price}</h6>
                    <RatingIcon rating={rating} />
                    <div className="mt-3">
                        <img className="product-img" src={imageUrl} />
                    </div>
                </div>
                <div className="card=footer">
                    <div className="row m-4">
                        <div className="col-6">
                            <div className="float-left">
                                <button type="button" onClick={onClick} className="btn btn-success">Add to cart</button>
                            </div>
                        </div>
                    
                        <div className="col-6">
                            <div className="float-right">
                                <button type="button" className="btn btn-outline-info">Checkout</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    )
}

export default Product
