import React from 'react'
import { useHistory  } from "react-router-dom";
import { Card } from 'antd';
import _default from 'antd/lib/time-picker';

const { Meta } = Card;

const StarredProduct = ({ imgUrl, productId, name , description }) => {
    let history = useHistory()

    const getProductDetail = () => {
        history.push(`/product-detail/${productId}`)
    }

    return (
        <div>
            <Card
                hoverable
                style={{ width: 240 }}
                cover={<img className="product-img" alt={name} src={imgUrl} />}
                onClick={() => getProductDetail(productId)}
            >
                <Meta title={name} description={description} />
            </Card>
        </div>
    )
}

export default StarredProduct
