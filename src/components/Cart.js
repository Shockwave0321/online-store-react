import React, { useContext, useState } from 'react';
import Product from './Product';
import { CartContext } from '../store/CartContext';

const Cart = props => {

    const [items, setItems] = useContext(CartContext)
    const btnAddToCart = useState(false)

    return (
        <div className="container">
            <ul className="product-container">
               
                    {
                        items.map(item => {
                            return <Product name={item.name} price={item.price} brand={item.brand} />
                        }) 
                    } 
                
            </ul> 
        </div>
    )
}

export default Cart
